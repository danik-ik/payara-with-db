FROM payara/server-full:5.183
EXPOSE 8700
COPY ./lib/* /opt/payara5/glassfish/domains/domain1/lib/ext/
COPY ./config/asadmin.commands /tmp/asadmin.commands
COPY ./bin/startInForeground.sh ${PAYARA_PATH}/bin/startInForeground.sh
RUN cat /tmp/asadmin.commands >> ${POSTBOOT_COMMANDS}
